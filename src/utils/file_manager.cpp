// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "file_manager.h"

FileManager::FileManager() {

}

FileManager::~FileManager() {

}

std::vector<std::string>
FileManager::get_content(std::string file_name) {
  std::ifstream file;
  file.open(file_name);
  std::vector<std::string> file_content;
  for (std::string line; getline(file, line);)
    file_content.push_back(line);

  file.close();
  return file_content;
}

void
FileManager::append(std::string file_name, std::string line) {
  std::ofstream file(file_name, std::ios::app | std::ios::out);
  file << line;
  file.close();
}

void
FileManager::overwrite(std::string file_name, std::vector<std::string> new_content) {
  std::ofstream file(file_name, std::ios::trunc | std::ios::out);
  file << " ";
  file.close();

  for(auto line: new_content)
    append(file_name, line + "\n");
}
