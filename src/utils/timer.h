#ifndef TIMER_H
#define TIMER_H
#include <chrono>
#include <cmath>

class Timer {
 public:
  typedef std::chrono::steady_clock::duration DeltaTime;
  typedef std::chrono::steady_clock::time_point Time;

  Timer();
  virtual ~Timer();
  float get_delta_time();
  void start();
  void stop();
  float get_time_since_start();

 private:
  Time last_time_, stop_;
  DeltaTime  delta_, freeze_time_;
  float start_;

  float time_to_float(DeltaTime time);
  float truncate(float number);
  Time now();
};

#endif