#ifndef PADDLE_HPP
#define PADDLE_HPP
#include <memory>
#include <OgreEntity.h>
#include <OgreSceneNode.h>
#include "physics.h"

class Paddle
{
public:
  Paddle();
  ~Paddle();

  void initialize(btRigidBody* body);

  void move(btVector3 direction);
  btVector3 get_position();

  btRigidBody* _body;
};


#endif
