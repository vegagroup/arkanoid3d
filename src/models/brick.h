#ifndef BRICK_HPP
#define BRICK_HPP
#include <memory>
#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "animation.h"
#include "physics.h"
#include "scene.h"
#include "player.h"

class Brick {
public:
  Brick();
  ~Brick();

  void initialize(btRigidBody* body, Ogre::SceneNode* node, Scene::shared scene,
  				Physics::shared physics, Player::shared player, Animation::shared animation);
  void collision();

  void remove_from_scene();

  btRigidBody* _body;
  Ogre::SceneNode* _node;

  Scene::shared _scene;
  Physics::shared _physics;
  Player::shared _player;
  Animation::shared _animation;
};


#endif
