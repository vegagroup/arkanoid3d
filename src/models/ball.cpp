#include "ball.h"

Ball::Ball(){
  _started = false;
}

Ball::~Ball(){

}

void
Ball::initialize(btRigidBody* body, Sound::shared sound, Scene::shared scene, Physics::shared physics){
  _body = body;
  _sound = sound;
  _scene = scene;
  _physics = physics;
}

void
Ball::reset(btVector3 position) {
  _started = false;

  _physics->move(_body, position);
  _physics->set_linear_velocity(_body,
                   btVector3(0, 0, 0));
}

void
Ball::move(btVector3 direction) {
  _body->setLinearVelocity(direction);
}

void
Ball::update() {
  if (_started) {
    btVector3 direction = get_direction();
    move(direction);
  }
}

void
Ball::start() {
  if(_started)
    return;

  _started = true;
  std::srand(std::time(0));
  int x_movement = (std::rand() % 8) - 4;
  std::cout << "random: " << x_movement << std::endl;
  move(btVector3(x_movement, 4, 0));
}

void
Ball::collide_with_brick(){
  _sound->play_fx("media/sound/explosion.wav");
}

void
Ball::collide_with_paddle(){
  _sound->play_fx("media/sound/key_pickup.wav");
}


btVector3
Ball::get_direction() {
  btVector3 direction = _body->getLinearVelocity();
  float dir_x = direction.x();
  float dir_y = direction.y();

  dir_y = correct_direction(dir_y, 4);
  dir_x = correct_direction(dir_x, 1);

  return btVector3(dir_x, dir_y, 0);
}

float
Ball::correct_direction(float direction, float correction) {
  if (direction >= 0 && direction < correction)
    return correction;
  if (direction < 0 && direction > -correction)
    return -correction;

  return direction;
}
