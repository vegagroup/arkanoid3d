#include "sound.h"

Sound::Sound() {
  _in_error_state = false;
  _current_filename = "";

  init_audio_device();
}

Sound::~Sound() {
  Mix_CloseAudio();
}

void
Sound::load(std::string type, std::string file_name) {
  if (type == "fx")
    acquire_fx(file_name.c_str());

  if (type == "music")
    acquire_music(file_name.c_str());
}

void
Sound::play_music(std::string file_name) {
  if (_in_error_state)
    return;

  if (is_stopped()){
    play(file_name);
    return;
  }
  if(is_paused()){
    Mix_ResumeMusic();
  }
}

void
Sound::play(std::string file_name) {
  try {
    Mix_Music* music = acquire_music(file_name);

    Mix_PlayMusic(music, -1);
    _current_filename = file_name.c_str();

  } catch (std::exception& e) {
    std::cerr << "Class Sound. Method play=> exception caught: " << e.what() << '\n';
  }
}

void
Sound::pause_music() {
  if (_in_error_state)
    return;

  Mix_PauseMusic();
}

void
Sound::stop_music() {
  if (_in_error_state)
    return;

  Mix_HaltMusic();
}

void
Sound::play_fx(std::string fileName) {
  if (_in_error_state)
    return;
  try {
    Mix_Chunk* fx = acquire_fx(fileName);
    Mix_PlayChannel(-1, fx, 0);
  }
  catch (std::exception& e) {
    std::cerr << "Class Sound. Method play_fx=> exception caught: " << e.what() << '\n';
  }
}

void
Sound::init_audio_device() {
  if (SDL_Init(SDL_INIT_AUDIO) == -1) {
    std::cerr << "Error initializing SDL audio subsystem...\n";
    _in_error_state = true;
    return;
  }

  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0) {
    std::cerr << "Error initializing audio device...\n";
    _in_error_state = true;
    return;
  }
}

void
Sound::play_next_track() {
  stop_music();
  play_music(next_music(_current_filename.c_str()));
}

void
Sound::enable_playlist() {
  //Mix_HookMusicFinished(std::bind(&Sound::play_next_track, this));
}

void
Sound::disable_playlist() {
  Mix_HookMusicFinished(NULL);
}


bool
Sound::is_paused() {
  return Mix_PausedMusic() == 1;
}

bool
Sound::is_stopped() {
  return !is_playing();
}

bool
Sound::is_playing() {
  return Mix_PlayingMusic() == 1;
}

Mix_Music*
Sound::acquire_music(std::string file_name) {
  if (_musics[file_name])
    return _musics[file_name];

  return load_music(file_name);
}

Mix_Music*
Sound::load_music(std::string file_name) {
  Mix_Music* music = Mix_LoadMUS(file_name.c_str());

  if (music != 0)
    _musics[file_name] = music;

  return music;
}

std::string
Sound::next_music(std::string file_name) {
  bool previous = false;
  for(auto& song: _musics) {
    if(previous)
      return song.first;
    if(file_name == song.first)
      previous = true;
  }
  std::string first_element = _musics.begin()->first;
  return first_element;
}

void
Sound::release_music(std::string file_name) {
  if(_musics[file_name]){
    Mix_FreeMusic(_musics[file_name]);
    _musics.erase(file_name);
  }
}

Mix_Chunk*
Sound::acquire_fx(std::string file_name) {
  if (_fx[file_name])
    return _fx[file_name];

  return load_fx(file_name);
}

Mix_Chunk*
Sound::load_fx(std::string file_name) {
  Mix_Chunk* fx_chunk = Mix_LoadWAV(file_name.c_str());

  if (fx_chunk != 0)
    _fx[file_name] = fx_chunk;

    return fx_chunk;
  }

void
Sound::release_fx(std::string file_name) {
  if(_fx[file_name]){
    Mix_FreeChunk(_fx[file_name]);
    _fx.erase(file_name);
  }
}
