#include "animation.h"

Animation::Animation() {

} 

Animation::~Animation() {

}

void
Animation::activate(Ogre::Entity* entity, std::string animation_name, 
      Animation::Callback callback) {

  Ogre::AnimationState* animation = entity->getAnimationState(animation_name);

  if(_active_animations[animation])
    return;

  animation->setEnabled(true);
  animation->setLoop(false);
  animation->setTimePosition(0.0);

  _active_animations[animation] = callback;
}

void
Animation::update(float deltaT) { 
  std::vector<Ogre::AnimationState*> for_delete;

  for (auto& animation: _active_animations) {
    animation.first->addTime(deltaT);

    if (animation.first->hasEnded())
      for_delete.push_back(animation.first);
  }

  for(auto& animation: for_delete) {
    _active_animations[animation]();
    _active_animations.erase(animation);
  }
}
