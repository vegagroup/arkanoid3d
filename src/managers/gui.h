#ifndef GUI_H
#define GUI_H
#include <memory>

#include <OgreOverlayContainer.h>
#include <OgreOverlayManager.h>
#include <OgreOverlayElement.h>
#include <OgreTextAreaOverlayElement.h>
#include <OgreFontManager.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

class GUI {
public:
  typedef std::shared_ptr<GUI> shared;

  GUI();
  virtual ~GUI();
  void switch_menu();
  void register_callback(std::string button, CEGUI::Event::Subscriber callback);

  CEGUI::Window* load_layout(std::string file);
  CEGUI::AnimationInstance* load_animation(std::string name, CEGUI::Window* window);
  Ogre::OverlayElement* create_overlay(std::string name, std::string element);
  Ogre::Overlay* create_overlay(std::string name);
  
  CEGUI::GUIContext& get_context();
  void inject_delta(float delta);

private:
  Ogre::OverlayManager* _overlay_manager;
  const std::string _default_font = "Manila-12";

  void load_resources();
  void init();
};


#endif
