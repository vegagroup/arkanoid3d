// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "gui.h"

GUI::GUI(){
  CEGUI::OgreRenderer::bootstrapSystem();
  _overlay_manager = Ogre::OverlayManager::getSingletonPtr();
  load_resources();
  init();
}

GUI::~GUI(){
  delete _overlay_manager;
}


void
GUI::load_resources() {
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
}

void
GUI::init() {
    CEGUI::SchemeManager::getSingleton().createFromFile("Arkanoid.scheme");
    CEGUI::GUIContext& _context = CEGUI::System::getSingleton().getDefaultGUIContext();
    _context.setDefaultFont(_default_font);
    _context.getMouseCursor().setPosition(CEGUI::Vector2f(0, 0));
    // CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();
    // animMgr.loadAnimationsFromXML("game.anims");
  }

CEGUI::Window*
GUI::load_layout(std::string file) {
  return CEGUI::WindowManager::getSingleton().loadLayoutFromFile(file);
}

CEGUI::AnimationInstance*
GUI::load_animation(std::string name, CEGUI::Window* window) {
  CEGUI::AnimationManager& animation_manager_ = CEGUI::AnimationManager::getSingleton();
  CEGUI::Animation* animation = animation_manager_.getAnimation(name);
  CEGUI::AnimationInstance* instance =
    CEGUI::AnimationManager::getSingleton().instantiateAnimation(animation);
  instance->setTargetWindow(window);

  return instance;
}

Ogre::OverlayElement*
GUI::create_overlay(std::string name, std::string element ){
  Ogre::Overlay *overlay = _overlay_manager->getByName(name);
  overlay->show();
  return _overlay_manager->getOverlayElement(element);
}

Ogre::Overlay*
GUI::create_overlay(std::string name){
  return _overlay_manager->getByName(name);
}

CEGUI::GUIContext&
GUI::get_context() {
    return CEGUI::System::getSingleton().getDefaultGUIContext();
}

void
GUI::inject_delta(float delta) {
  CEGUI::GUIContext&
    defaultGUIContext(CEGUI::System::getSingleton().getDefaultGUIContext());
  defaultGUIContext.injectTimePulse(delta);
}
