#include "game.h"

Game::Game() {
  _scene = std::make_shared<Scene>();
  _input = std::make_shared<EventListener>(_scene->_window);
  _gui = std::make_shared<GUI>();
  _sound = std::make_shared<Sound>();
  _animation = std::make_shared<Animation>();
  _player = std::make_shared<Player>(11);
}

Game::~Game() {
}

void
Game::start() {
  _state_table["play"] = std::make_shared<Play>(shared_from_this());
  _state_table["pause"] = std::make_shared<Pause>(shared_from_this());
  _state_table["menu"] = std::make_shared<Menu>(shared_from_this());
  _state_table["results"] = std::make_shared<Results>(shared_from_this());

  set_current_state("menu");
  game_loop();
}

void
Game::set_current_state(std::string next_state) {
  _current_state = next_state;
  _state_table[next_state]->init();
}

bool
Game::has_ended() {
  return _player->has_died() || _player->all_bricks_deleted();
}

void
Game::load_sound() {
  _sound->load("music", "media/sound/windloop.ogg");
  _sound->load("music", "media/sound/In_The_Name_Of_All_SCC.wav");

  _sound->load("fx", "media/sound/nightmare.wav");
  _sound->load("fx", "media/sound/explosion.wav");
  _sound->load("fx", "media/sound/key_pickup.wav");
}

void
Game::game_loop() {
  _timer.start();
  while(!_input->_exit) {
    _delta += _timer.get_delta_time();
    _input->capture();
    if(_delta >= (1/FPS)) {
      _input->check_events();
      _state_table[_current_state]->update();
      _scene->render_one_frame();
      _delta = 0.f;
    }
  }
}
