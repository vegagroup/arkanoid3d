#ifndef PAUSE_H
#define PAUSE_H
#include "state.h"

class Pause: public State {
  CEGUI::Window* _menu_window;

public:
  typedef std::shared_ptr<Pause> shared;
  Pause(std::shared_ptr<Game> game);
  virtual ~Pause();

  void init();
  void exit();
  void update();

 private:
  bool change_state(const CEGUI::EventArgs &event);

  void add_hooks();
  void add_gui_hooks();
  void add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
};
#endif
