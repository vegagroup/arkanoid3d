#include "game.h"

Play::Play(Game::shared game): State(game) {
  _game->load_sound();
  _init = false;
}

Play::~Play() {
}

void
Play::init() {
  _game->_sound->play_music("media/sound/In_The_Name_Of_All_SCC.wav");
  init_session();
  add_callbacks();
}

void
Play::exit() {
  State::_game->_input->clear_hooks();
  State::_game->_sound->pause_music();
}

void
Play::pause() {
  exit();
  State::_game->set_current_state("pause");
}

void
Play::end_game() {
  _init = false;
  exit();

  if(_game->has_ended()){
    _session->reset_overlays();
    State::_game->set_current_state("results");
  }

}

void
Play::add_callbacks() {
  _game->_input->add_hook({OIS::KC_ESCAPE, true},
                          EventType::doItOnce,
                          std::bind(&Play::pause, this));

  State::_game->_input->add_hook({OIS::KC_A, true}, EventType::repeat,
                   std::bind(&Session::move, _session.get(),
                   "paddle", btVector3(-5, 0, 0)));
  State::_game->_input->add_hook({OIS::KC_A, false}, EventType::repeat,
                   std::bind(&Session::move, _session.get(),
                   "paddle", btVector3(0, 0, 0)));
  State::_game->_input->add_hook({OIS::KC_D, true}, EventType::repeat,
                   std::bind(&Session::move, _session.get(),
                   "paddle", btVector3(5, 0, 0)));
  State::_game->_input->add_hook({OIS::KC_D, false}, EventType::repeat,
                   std::bind(&Session::move, _session.get(),
                   "paddle", btVector3(0, 0, 0)));

  State::_game->_input->add_hook({OIS::KC_1, false}, EventType::doItOnce,
                   std::bind(&Session::start, _session.get()));
}


void
Play::resume() {

}

void
Play::update() {
  _session->update(_game->_delta);

  if(_session->has_finished())
    end_game();
}

void
Play::init_session() {
   if(_init){
    _session->show_overlays();
    return;
  }
std::cout << __func__ << std::endl;
  _init = true;

  _session.reset();
  Session::shared new_session = std::make_shared<Session>();
  _session.swap(new_session);

  _game->_player->reset();

  _session->initialize(State::_game->_scene, State::_game->_sound,
      State::_game->_gui, State::_game->_player, State::_game->_animation);
}
