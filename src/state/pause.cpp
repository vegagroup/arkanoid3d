// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "game.h"

Pause::Pause(std::shared_ptr<Game> game): State(game) {
  _menu_window = State::_game->_gui->load_layout("Pause.layout");
  _menu_window->hide();
}

Pause::~Pause() {
  delete _menu_window;
}

void
Pause::init() {
  add_hooks();
  _menu_window->setVisible(true);

  add_gui_hooks();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(_menu_window);
}

void
Pause::exit() {
  State::_game->_input->clear_hooks();
  _menu_window->setVisible(false);
  State::_game->set_current_state("play");
}


bool
Pause::change_state(const CEGUI::EventArgs &event) {
  exit();
  return true;
}

void
Pause::update() {

}

void
Pause::add_hooks() {
  State::_game->_input->add_hook({OIS::KC_ESCAPE, true}, EventType::doItOnce,
                          std::bind(&Pause::exit, this));
}

void
Pause::add_gui_hooks() {
  add_hooks("Exit", CEGUI::Event::Subscriber(&EventListener::gui_shutdown,
                                           State::_game->_input.get()));
  add_hooks("Resume", CEGUI::Event::Subscriber(&Pause::change_state, this));
}

void
Pause::add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  _menu_window->getChild(button)->
      subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}
