// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "game.h"
#include "file_manager.h"

Results::Results(std::shared_ptr<Game> game): State(game) {
  menu_window_ = _game->_gui->load_layout("Results.layout");
}

Results::~Results() {
  delete menu_window_;
}

void
Results::exit() {

}


void
Results::init() {
  add_hooks();

  add_gui_hooks();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);

  update_menu(); 

  if(_game->_player->has_died())
    return;

  insert_record_to_file();
}

bool
Results::change_state(const CEGUI::EventArgs &event) {
  _game->_input->clear_hooks();
  _game->set_current_state("menu");
  return true;
}

void
Results::add_hooks() {
  _game->_input->add_hook({OIS::KC_ESCAPE, true}, EventType::doItOnce,
                          std::bind(&EventListener::shutdown, _game->_input));
}

void
Results::add_gui_hooks() {
  add_hooks("Exit", CEGUI::Event::Subscriber(&Results::change_state,
                                             this));
}

void
Results::add_hooks(const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  menu_window_->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

void
Results::update() {

}

void
Results::update_menu() {
  if (_game->_player->has_died()) {
    menu_window_->getChild("Title")->setText("Game Over");
    menu_window_->getChild("Title")->setPosition(
        CEGUI::UVector2(CEGUI::UDim(0.06, 0), CEGUI::UDim(0, 0)));
  } 
  else {
    menu_window_->getChild("Title")->setText("Results");
    menu_window_->getChild("Title")->setPosition(
        CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0, 0)));  
  }

  menu_window_->getChild("Time")->
    setText(to_string_with_precision(_game->_player->get_time_counter(), 3));
  menu_window_->getChild("Score")->
    setText(to_string_with_precision(_game->_player->get_score(), 3));
  menu_window_->getChild("Life")->
    setText(to_string_with_precision(_game->_player->get_lifes_number(), 3));
}

std::string
Results::to_string_with_precision(const float number, const int n) {
    std::ostringstream number_stringify;
    number_stringify << std::setprecision(n) << number;
    return number_stringify.str();
}

void
Results::insert_record_to_file() {
  std::cout << __func__ << std::endl;
  FileManager file_manager;
  file_manager.append("config/records.txt", _game->_player->to_string());

  std::vector<std::string> content =
    file_manager.get_content("config/records.txt");
  std::sort(content.begin(), content.end(), [&](std::string i, std::string j){
      return (j > i);
    });

  file_manager.overwrite("config/records.txt", content) ;
}
