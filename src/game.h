#ifndef GAME_HPP
#define GAME_HPP
#include <memory>

#include "play.h"
#include "pause.h"
#include "menu.h"
#include "results.h"

#include "timer.h"

class Game: public std::enable_shared_from_this<Game> {
  typedef std::map<std::string, State::shared> StateTable;
  const float FPS = 60;

  StateTable _state_table;
  Timer _timer;

  std::string _current_state;

 public:
  typedef std::shared_ptr<Game> shared;

  float _delta;

  Scene::shared _scene;
  EventListener::shared _input;
  GUI::shared _gui;
  Sound::shared _sound;
  Animation::shared _animation;
  Player::shared _player;

  Game();
  virtual ~Game();

  bool has_ended();
  void start();

  void set_current_state(std::string next_state);
  void load_sound();
  bool init_SDL();

 private:
  void game_loop();
};

#endif
